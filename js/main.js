$('.tab_content img').each(function(){
	var theSRC = $(this).attr('src');
	$(this).attr('src', '');
	$(this).data('src', theSRC);
});

$(document).ready(function() {
	var $iframe = $('.mainVideo .videoTemplate').html();

	$('.mainVideo').click(function(){
		$(this).html($iframe);
	});

	/*
	var iframe = $('.mainVideo').html();

	$('.mainVideo').click(function(){
		$(this).html(iframe).find('iframe').show();
	}).find('iframe').remove();
	*/

	$('.forgotPass').click(function(event){
		event.preventDefault();
		var		width = 420,
				height = 250,
				top = 100,
				left = 200;
		var properties = "status=no,location=no,toolbar=no,directories=no,resizable=yes,width=" +width+ ",height=" +height+", top=" +top+ ", left=" +left;
		window.open('https://oss.ticketmaster.com/html/forgotPassword.htmI?team=clippers&l=EN&AcctId=', "", properties);
	});
 
	//Default Action
	$("ul.tabs li:first").addClass("active"); //Activate first tab
	
	//On Click Event
	$("ul.tabs li").click(function() {

		if($(this).find('a').attr('href') == '#tab1' ) {
			$('html').addClass('home-tab');
		} else {
			$('html').removeClass('home-tab');
		}

		$("ul.tabs li").removeClass("active"); //Remove any "active" class
		$(this).addClass("active"); //Add "active" class to selected tab
		$(".tab_content").hide(); //Hide all tab content

		var activeTab = $(this).find("a").attr("href"); //Find the rel attribute value to identify the active tab + content

		$(activeTab).find('img').each(function(){
			var theSRC = $(this).data('src');
			$(this).attr('src', theSRC);
		});

		$(activeTab).fadeIn(); //Fade in the active content

		return false;
	});
 	
	// begin using timer on 3/16/2013
	// countdown should count back from number in span
	var 	timeToStart = new Date("March 14, 2013 00:00:00").getTime(),
			currentTime = new Date().getTime();

	//testing
	//timeToStart = new Date("January 16, 2013 00:00:00").getTime()

 	if( currentTime > timeToStart ) {
	 	$(".countdown").fadeIn().kkcountdown({
	    	dayText		: '',
	    	daysText 	: '',
	        hoursText	: '',
	        minutesText	: '',
	        secondsText	: '',
	        displayZeroDays : false,
	        oneDayClass	: 'one-day'
	    });
	}
});